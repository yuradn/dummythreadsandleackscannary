package yurasik.com.fivethreads;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.squareup.leakcanary.RefWatcher;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements DummyGoInterface {

    private Thread[] dummyThreads;
    private int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            startAsyncTask();
            }
        });



    }

    @Override
    protected void onStop() {
        super.onStop();
        RefWatcher refWatcher = App.getRefWatcher(this);
        refWatcher.watch(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onComplete(int index) {
        LogUtil.i("Thread: " + index);

        synchronized (this) {
            count--;
        }

        if (count == 0) {
            LogUtil.i("Work complete.");
        }
    }

    @SuppressLint("StaticFieldLeak")
    void startAsyncTask() {
        // This async task is an anonymous class and therefore has a hidden reference to the outer
        // class MainActivity. If the activity gets destroyed before the task finishes (e.g. rotation),
        // the activity instance will leak.
        new AsyncTask<Void, Void, Void>() {
            @Override protected Void doInBackground(Void... params) {
                // Do some slow work in background
                count = 50;
                dummyThreads = new Thread[count];
                for (int i = 0; i < dummyThreads.length; i++) {
                   /* DummyThread dummyThread = new DummyThread(MainActivity.this, i);*/
                   /* dummyThreads[i] = dummyThread;*/
                    final int finalI = i;
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Random random = new Random();
                            long delay = random.nextInt(10) * 1000;
                            LogUtil.i("Thread :" + finalI + " delay: " + delay);
                            try {
                                Thread.sleep(delay);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            //LogUtil.i("Thread :" + finalI + " stopped.");
                            MainActivity.this.onComplete(finalI);
                        }
                    });
                    dummyThreads[i] = thread;
                }

                for (int i = 0; i < dummyThreads.length; i++) {
                    dummyThreads[i].start();
                }


                //SystemClock.sleep(20000);
                return null;
            }
        }.execute();
    }
}
