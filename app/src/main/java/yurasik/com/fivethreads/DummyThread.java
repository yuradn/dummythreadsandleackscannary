package yurasik.com.fivethreads;

import java.util.Random;

public class DummyThread extends Thread {

    private DummyGoInterface goInterface;
    private DummyRunnable dummyRunnable;
    private int index;
    private long delay = 0;

    public DummyThread(DummyGoInterface goInterface, int index) {
        this.goInterface = goInterface;
        this.index = index;
        delay = new Random().nextInt(10) * 1000;
        dummyRunnable = new DummyRunnable(delay);
        LogUtil.i("Index: "+index+" delay: "+delay);
    }

    @Override
    public synchronized void start() {
        super.start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                dummyRunnable.run();
            }
        });
    }

    public void setGoInterface(DummyGoInterface goInterface) {
        this.goInterface = goInterface;
    }

    public DummyGoInterface getGoInterface() {
        return goInterface;
    }



    class DummyRunnable implements Runnable {

        private long delay = 0;

        public DummyRunnable(long delay) {
            this.delay = delay;
        }

        @Override
        public void run() {
            work();
        }

        private void work() {
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            goInterface.onComplete(index);
        }
    }
}